function limitFunctionCallCount(cb, n){
    let limit = 0;
    return function invokeCb() {
        if(limit < n){
            limit++;
            return cb();
        }else {
            //console.log('limit exceeds')
            return null;
        }
    }
}

module.exports = limitFunctionCallCount;
