const cacheFunction = require('../cacheFunction');
//callback function
function callback() {
    let num = Math.floor(Math.random()*100);
    return num;
}
let test = cacheFunction(callback);
//calling invokeCb with first argument
let output1 = test('first');
//calling invokeCb with same argument
let output2 = test('first');
if(output1 === output2){
    console.log('Code is running');
}else {
    console.log('There is some issue');
}
