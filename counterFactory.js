function counterFactory() {
    // * Return an object that has two methods called `increment` and `decrement`.
    return {
        increment: function increment(n) {
        // Todo => `increment` should increment a counter  and return it.
            n += 1;
            return n;
        },
        decrement: function decrement(n) {
        // Todo => `decrement` should decrement the counter  and return it.
            n -= 1;
            return n;
        }
    }
}
// let increment = counterFactory().increment(3)
// console.log(increment)
// let decrement = counterFactory().decrement(3)
// console.log(decrement)

module.exports = counterFactory();